package com.inkglobal.techtest;

public class BerlinClock {

	public String getLamps(String time) {
		String[] components = time.trim().split(":");
		
		int hour = Integer.parseInt(components[0]);
		int minutes = Integer.parseInt(components[1]);
		int seconds = Integer.parseInt(components[2]);

		return new StringBuilder()
			.append(seconds % 2 == 0 ? "Y" : "O")
			.append(" ")
			.append(getShortLamps(hour / 5, "R"))
			.append(" ")
			.append(getShortLamps(hour % 5, "R"))
			.append(" ")
			.append(getLongLamps(minutes / 5))
			.append(" ")
			.append(getShortLamps(minutes % 5, "Y"))
			.toString();
	}
	
	StringBuilder getShortLamps(int count, String color) {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = 0; i < count; i++)
			stringBuilder.append(color);
		
		return oPad(stringBuilder, 4);
	}
	
	StringBuilder getLongLamps(int count) {
		StringBuilder stringBuilder = new StringBuilder();
		
		for (int i = 1; i <= count; i++)
			if (i % 3 == 0)
				stringBuilder.append("R");
			else
				stringBuilder.append("Y");
		
		return oPad(stringBuilder, 11);
	}
	
	StringBuilder oPad(StringBuilder stringBuilder, int totalLength) {
		while (stringBuilder.length() < totalLength)
			stringBuilder.append("O");
		
		return stringBuilder;
	}
}
