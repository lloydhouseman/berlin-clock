package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BerlinClockTest {

	@Test
	public void testGetLamps() {
		BerlinClock berlinClock = new BerlinClock();
		
		assertEquals("Y OOOO OOOO OOOOOOOOOOO OOOO", berlinClock.getLamps("00:00:00"));
		assertEquals("O RROO RRRO YYROOOOOOOO YYOO", berlinClock.getLamps("13:17:01"));
		assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY", berlinClock.getLamps("23:59:59"));
		assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO", berlinClock.getLamps("24:00:00"));
	}
}
